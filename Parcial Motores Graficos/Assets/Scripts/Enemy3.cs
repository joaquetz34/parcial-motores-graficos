using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy3 : MonoBehaviour
{

    bool tengoQueMoverme = false;
    int rapidez = 5;

    void Update()
    {
        if (transform.position.z <= 7)
        {
            tengoQueMoverme = true;
        }
        if (transform.position.z >= 50)
        {
            tengoQueMoverme = false;
        }

        if (tengoQueMoverme)
        {
            MoverIzquierda();
        }
        else
        {
            MoverDerecha();
        }

    }

    void MoverIzquierda() 
    {
        //    transform.position += transform.right * rapidez * Time.deltaTime; 
        transform.Translate(0, 0, 1);
    }


    void MoverDerecha()
    {
        // transform.position -= transform.right * rapidez * Time.deltaTime;
        transform.Translate(0, 0, -1);
    }

    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag == "Jugador")
        {

            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }
    }

}