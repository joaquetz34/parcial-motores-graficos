using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy5 : MonoBehaviour
{

    bool tengoQueBajar = false;
    int rapidez = 5;

    void Update()
    {
        if (transform.position.y >= 8)
        {
            tengoQueBajar = true;
        }
        if (transform.position.y <= -4)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }

    void Subir()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }

    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag == "Jugador")
        {

            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }
    }


}