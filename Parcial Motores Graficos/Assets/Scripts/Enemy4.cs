using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy4 : MonoBehaviour
{

    bool Subiendo = false;
    public int velocidad = 10;

    private void Update()
    {
        if (transform.position.y >= 7)
        {
            Subiendo = true;
        }
        if (transform.position.y <= -10)
        {
            Subiendo = false;
        }

        if (Subiendo)
        {
            Bajo();
        }
        else
        {
            Subo();
        }

    }

    void Subo()
    {
        transform.position += transform.up * velocidad * Time.deltaTime;
    }

    void Bajo()
    {
        transform.position -= transform.up * velocidad * Time.deltaTime;
    }

    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag == "Jugador")
        {

            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }
    }

}

