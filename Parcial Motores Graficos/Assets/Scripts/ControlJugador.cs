using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class ControlJugador : MonoBehaviour
{
    public bool SeAcaboElTiempo; 
    public float rapidezDesplazamiento = 10.0f;
    public float threshold;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;
    private Rigidbody rb;
    public float saltover;
    private bool EnElPiso = true;
    public int maxSaltos = 2;
    public int saltoAhora = 0;
    public TMPro.TMP_Text textoCantidadRecolectados;
    public TMPro.TMP_Text textoGanaste;
    public TMPro.TMP_Text textoPerdiste;
    private int cont;
    public float timeRemaining = 10;
    public bool Paused;
    public TMPro.TMP_Text TimerTxt;
    public int seconds;
    public int minutes;
    public void PauseGame()
    {
     Time.timeScale = 0;
    }
    public void ResumeGame()
    {
     Time.timeScale = 1;
    }
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        cont = 0;
        textoGanaste.text = "";
        textoPerdiste.text = "";
        setearTextos();
        SeAcaboElTiempo = false;
        ResumeGame();
        Paused = false;
        TimerTxt.text = "";
        minutes = 1;
        seconds = 60; 
    }

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento; //Movimiento
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

     
         if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso())
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
        };
       
        if (Input.GetKeyDown(KeyCode.Space) && (EnElPiso || maxSaltos > saltoAhora))
        {
            rb.velocity = new Vector3(0f, saltover, 0f * Time.deltaTime);
            EnElPiso = false;
            saltoAhora++;
        };

        if (Input.GetKeyDown(KeyCode.P))
        {
            
            if (Paused)
            {
              ResumeGame();
            }
            else
            {
                PauseGame();
            }
            Paused = !Paused;
        }
        if (timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;
            TimerTxt.text = "Tiempo Restante: " + timeRemaining.ToString("n2");
        }
        else 
        {
            SeAcaboElTiempo = true;
            Debug.Log("Se acab� el tiempo. Game Over");
            setearTextos();
            PauseGame();
        }
       
    }
    private void setearTextos()
    {
        textoCantidadRecolectados.text = "Cantidad recolectados: " + cont.ToString();
        if (cont >= 11)
        {
            textoGanaste.text = "Ganaste!";
        }
        if (SeAcaboElTiempo)
        {
            textoPerdiste.text = "Perdiste ";
        }
        
        
        }
    
    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
}

   

        
    private void OnCollisionEnter(Collision collision)
    {
        EnElPiso = true;
        saltoAhora = 0;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Coleccionable") == true)
        {
            cont = cont + 1;
            setearTextos();
            other.gameObject.SetActive(false);
        }
        else if (other.gameObject.CompareTag("PowerUp") == true)
        {
            this.transform.localScale = new Vector3(transform.localScale.x * 2, transform.localScale.y * 2, transform.localScale.z * 2);
            rapidezDesplazamiento *= 2;
            other.gameObject.SetActive(false);
        }

}
   

void updateTimer(float currentTime)
    {
    float minutes = Mathf.FloorToInt(currentTime / 60);
    float seconds = Mathf.FloorToInt(currentTime % 60);
}
}