using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FallOutOfMap : MonoBehaviour
{
    GameObject Jugador;
    void Start()

    {
        Jugador = GameObject.Find("Jugador");
    }

    void Update()
    {
        if (Jugador.transform.position.y <= -1)
        {
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }
}
}
